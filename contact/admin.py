from django.contrib import admin
from contact.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin
class ContactAdmin(admin.ModelAdmin, FrontendEditableAdminMixin):
    # ...
    list_display = ('first_name','last_name', 'hear_about_us', 'email', 'phone', 'time_to_call','message','date_added')

admin.site.register(Contact, ContactAdmin)
