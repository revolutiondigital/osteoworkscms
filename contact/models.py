
from django.db import models
from cms.models import CMSPlugin

class Contact(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	phone = models.CharField(max_length=200)
	time_to_call = models.CharField(max_length=200)
	hear_about_us = models.CharField(max_length=200)
	message = models.TextField()
	date_added = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.first_name + self.last_name
