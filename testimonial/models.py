from django.db import models
from cms.models import CMSPlugin
class Testimonial(models.Model):
	name = models.CharField(max_length=200)
	content = models.TextField()
	date = models.CharField(max_length = 200, blank = True, null  = True)
	video_url = models.CharField(max_length=400, blank = True, null = True, verbose_name=u'Youtube URL(end tag only) i.e SKlbCjNCDn4') 

	def __unicode__(self):
		return self.name
class TestimonialPluginModel(CMSPlugin):
    testimonial = models.ForeignKey('testimonial.Testimonial', related_name='plugins')

    def __unicode__(self):
      return self.testimonial.name