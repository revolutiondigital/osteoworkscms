# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader

from testimonial.models import Testimonial

def index(request):
    testimonials = Testimonial.objects.filter()
    template = loader.get_template('index.html')
    context = RequestContext(request, {
        'testimonials': testimonials,
    })
    return HttpResponse(template.render(context))
