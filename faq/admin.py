from django.contrib import admin
from faq.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin
class FAQAdmin(admin.ModelAdmin, FrontendEditableAdminMixin):
    # ...
    list_display = ('question','answer')

admin.site.register(FAQ, FAQAdmin)
