
from django.db import models
from cms.models import CMSPlugin
class FAQ(models.Model):
	question = models.TextField()
	answer = models.TextField()

	def __unicode__(self):
		return self.question
class FAQPluginModel(CMSPlugin):
    faq = models.ForeignKey('faq.FAQ', related_name='plugins')

    def __unicode__(self):
      return self.faq.question