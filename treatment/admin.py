from django.contrib import admin
from treatment.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin
class ConditionAdmin(admin.ModelAdmin, FrontendEditableAdminMixin):
    # ...
    list_display = ('condition','cure')

admin.site.register(Condition, ConditionAdmin)
