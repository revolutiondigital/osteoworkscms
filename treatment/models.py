
from django.db import models
from cms.models import CMSPlugin
class Condition(models.Model):
	condition = models.TextField()
	cure = models.TextField()

	def __unicode__(self):
		return self.condition
class ConditionPluginModel(CMSPlugin):
    condition = models.ForeignKey('treatment.Condition', related_name='plugins')

    def __unicode__(self):
      return self.condition.condition
