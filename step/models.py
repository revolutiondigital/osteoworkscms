
from django.db import models
from cms.models import CMSPlugin
class Step(models.Model):
	step_number = models.CharField(max_length=200)
	title = models.CharField(max_length = 200 )
	description = models.TextField()
	image = models.ImageField(upload_to="uploads/stepimages", verbose_name=u'picture must be y x y(square) pixels')
	def __unicode__(self):
		return self.title

class StepPluginModel(CMSPlugin):
    step = models.ForeignKey('step.Step', related_name='plugins')

    def __unicode__(self):
      return self.step.title
