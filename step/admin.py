from django.contrib import admin
from step.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class StepAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('step_number', 'description', 'title')
admin.site.register(Step, StepAdmin)
