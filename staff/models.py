from django.db import models
from cms.models import CMSPlugin

class Staff(models.Model):
	name = models.CharField(max_length=200)
	content = models.TextField()
	profileimage = models.ImageField(upload_to="uploads/staffpicture", verbose_name=u'picture must be y x y(square) pixels')
	def __unicode__(self):
		return self.name

class StaffPluginModel(CMSPlugin):
    staff = models.ForeignKey('staff.Staff', related_name='plugins')

    def __unicode__(self):
      return self.staff.name