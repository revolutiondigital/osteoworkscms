from django.utils.translation import ugettext as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import StaffPluginModel

class StaffPlugin(CMSPluginBase):
    model = StaffPluginModel                 # Model where data about this plugin is saved
    name = _("Staff Plugin")                 # Name of the plugin
    render_template = "plugin.html"   # template to render the plugin with

    def render(self, context, instance, placeholder):
        context.update({'instance':instance})
        return context

plugin_pool.register_plugin(StaffPlugin)        