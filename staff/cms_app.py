# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

class StaffApp(CMSApp):
    name = _("Staff App")        # give your app a name, this is required
    urls = ["staff.urls"]       # link your app to url configuration(s)
    app_name = "staff"          # this is the application namespace

apphook_pool.register(StaffApp) # register your app