from django.contrib import admin
from staff.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin

class StaffAdmin(admin.ModelAdmin,FrontendEditableAdminMixin ):
    # ...
    list_display = ('name', 'content')
admin.site.register(Staff, StaffAdmin)
