from django.http import HttpResponse
from django.template import RequestContext, loader

from staff.models import Staff

def index(request):
    staff = Staff.objects.filter()
    template = loader.get_template('index.html')
    context = RequestContext(request, {
        'staff': staff,
    })
    return HttpResponse(template.render(context))
