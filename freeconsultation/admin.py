from django.contrib import admin
from freeconsultation.models import *
from cms.admin.placeholderadmin import FrontendEditableAdminMixin
class FreeConsultationAdmin(admin.ModelAdmin, FrontendEditableAdminMixin):
    # ...
    list_display = ('first_name','last_name', 'email', 'phone', 'time_to_call','message', 'osteoporosis', 'osteopenia', 'joint_back_pain', 'better_balance', 'improve_strength', 'other', 'date_added')

admin.site.register(FreeConsultation, FreeConsultationAdmin)
