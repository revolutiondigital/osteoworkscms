from django.db import models
from cms.models import CMSPlugin

class FreeConsultation(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	phone = models.CharField(max_length=200)
	time_to_call = models.CharField(max_length=200)
	message = models.TextField()
	osteoporosis = models.BooleanField(default=False)
	osteopenia= models.BooleanField(default=False)
	joint_back_pain= models.BooleanField(default=False)
	better_balance= models.BooleanField(default=False)
	improve_strength= models.BooleanField(default=False)
	other= models.BooleanField(default=False)
	date_added = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.first_name + self.last_name
