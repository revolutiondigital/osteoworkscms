
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.http import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from contact.models import *
from freeconsultation.models import *

def handle404(request):
	return render_to_response("404.html",  context_instance=RequestContext(request))

def handle500(request):
	return render_to_response("500.html",  context_instance=RequestContext(request))	

def contact(request):
	form_args={}
	form_args['contact'] = True
	print request.method
	if request.method == "POST":

		first_name = request.POST.get('first_name')
		last_name = request.POST.get('last_name')
		email = request.POST.get('email')
		how_find_us = request.POST.get('hear_about_us')
		phone = request.POST.get('phone')
		time_to_call = request.POST.get('time-to-call')
		message = request.POST.get('message')

		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Message from client"
		msg['From']    = "info <info@osteoworks.ca>" # Your from name and email address
		msg['To']      = "info@osteoworks.ca"

		text = "Name:" + first_name+" "+ last_name +"\n"+"Phone:" + phone + "\n"+"Email:"+ email+ "\n"+"How Did They Find Us?:"+ how_find_us + "\n"+message
		part1 = MIMEText(text, 'plain')
		msg.attach(part1)

		


		try:
   			smtpObj = smtplib.SMTP('in-v3.mailjet.com',25)
   			smtpObj.login('0b9fc98c564c16c97d61801702a219b6','da8e2e0b7d76a6df4c37f759cbc80e98' )
			smtpObj.sendmail(msg['From'], msg['To'], msg.as_string())       
			smtpObj.quit()
			print "Successfully sent email"

		except SMTPException:
			print "Error: unable to send email"
	

	
		contact = Contact(first_name = first_name,
						  last_name = last_name,
						  email = email,
						  phone = phone,
						  time_to_call = time_to_call,
						  hear_about_us = how_find_us,
						  message = message)
		contact.save()				  	
		form_args['success_message'] = True
	return HttpResponseRedirect("/contact/?message=success")



def book_consultation(request):
	form_args={}
	form_args['contact'] = True
	print request.method
	if request.method == "POST":

		first_name = request.POST.get('first_name')
		last_name = request.POST.get('last_name')
		email = request.POST.get('email')
		phone = request.POST.get('phone')
		time_to_call = request.POST.get('time-to-call')
		message = request.POST.get('message')
		osteoporosis = request.POST.get('osteoporosis')
		osteopenia= request.POST.get('osteopenia')
		joint_back_pain= request.POST.get('joint_back_pain')
		better_balance= request.POST.get('better_balance')
		improve_strength= request.POST.get('improve_strength')
		improve_posture= request.POST.get('improve_posture')
		other= request.POST.get('other')
		symptoms = ""

		if osteoporosis == "on":
			osteoporosis = True
			symptoms = symptoms + "osteoporosis, "
		else:
			osteoporosis = False	
		
		if osteopenia == "on":
			osteopenia = True
			symptoms = symptoms + "osteopenia, "
		else:
			osteopenia = False
		
		if joint_back_pain == "on":
			joint_pain = True
			symptoms = symptoms + "joint integrity, "
		else:
			joint_pain = False
		
		if better_balance == "on":
			better_balance = True
			symptoms = symptoms + "better balance, "
		else:
			better_balance = False
		
		if improve_strength == "on":
			improve_strength = True
			symptoms = symptoms + "muscular strength, "
		else:
			improve_strength = False

		if improve_posture == "on":
			symptoms = symptoms + "improve posture, "
		else:
			improve_posture = False	
		
		if other == "on":
			other = True
			symptoms = symptoms + "other, "
		else:
			other = False				
		
		print symptoms	
		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Client Wants To Book Free Consultation"
		msg['From']    = "info <info@osteoworks.ca>" # Your from name and email address
		msg['To']      = "info@osteoworks.ca"

		text = "Name:" + first_name+" "+ last_name +"\n"+"Phone:" + phone + "\n"+"Email:"+ email + "\n"+"Synptoms:"+symptoms+"\n" +message
		part1 = MIMEText(text, 'plain')
		msg.attach(part1)

		


		try:
   			smtpObj = smtplib.SMTP('in-v3.mailjet.com',25)
   			smtpObj.login('0b9fc98c564c16c97d61801702a219b6','da8e2e0b7d76a6df4c37f759cbc80e98' )
			smtpObj.sendmail(msg['From'], msg['To'], msg.as_string())       
			smtpObj.quit()
			print "Successfully sent email"

		except SMTPException:
			print "Error: unable to send email"
	

	
		freeconsultation = FreeConsultation(first_name = first_name,
						  last_name = last_name,
						  email = email,
						  phone = phone,
						  time_to_call = time_to_call,
						  message = message,
						  osteoporosis = osteoporosis,
						  osteopenia = osteopenia,
						  joint_back_pain = joint_pain,
						  better_balance = better_balance,
						  improve_strength = improve_strength,
						  other = other)
		freeconsultation.save()				  	
		form_args['success_message'] = True
	return HttpResponseRedirect("/contact/book-free-consultation/?message=success")
