from django.conf.urls import *
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from cms.sitemaps import CMSSitemap
from django.views.generic import RedirectView

handler404 = 'osteoworkscms.views.handle404'
handler500 = 'osteoworkscms.views.handle500'
urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^contact/book-appointment/$', RedirectView.as_view(url='https://osteoworks.acuityscheduling.com/')),
    url(r'^sendemail$', 'osteoworkscms.views.contact'),
    url(r'^bookconsultation$', 'osteoworkscms.views.book_consultation'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^', include('cms.urls')),
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
) + staticfiles_urlpatterns() + urlpatterns
